/*
 *  Subscribe Directive Module
 *      - runs subscription utilities
 */
var scripts = document.getElementsByTagName("script")
var currentScriptPath = scripts[scripts.length - 1].src;

angular.module('app').directive('subscribeUser', [
    function () {
        return {
            scope: true,
            templateUrl: "modules/subscribe/subscribeUser.template.html",
            controller: 'SubscriptionController'
        }
    }
]).controller('SubscriptionController',['$scope', 'SubscribeService', 
    function ($scope, SubscribeService){
        $scope.email;
        $scope.subscribeUser = function (email) {
            //console.log('Email to be subscribed', email);
            SubscribeService.subscribeNew(email, function (response) {
                //console.log('Response from the Subscribtion Service', response);
                alert(response);
            }, function (error) {
                //console.log('Error found:', error);
                alert(error);
            });
        };
}]);