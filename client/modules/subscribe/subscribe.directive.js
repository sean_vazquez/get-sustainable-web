/*
 *  Subscribe Directive Module
 *      - runs subscription interface
 */

angular.module('app').directive('subscribeModule', [
    function () {
        return {
            scope: true,
            templateUrl: 'modules/subscribe/subscribe.template.html',
            //Embed a custom controller in the directive
            //controller: SubscribeModuleController,
            link: function (scope, element, attrs) {
                var ctrl = this;
                var toggleEmailPrompt = function (value) {
                    ctrl.showEmailPrompt = value;
                };

                var toggleUsernamePrompt = function (value) {
                    ctrl.showUsernamePrompt = value;
                };

                ctrl.showEmailPrompt = false;
                ctrl.showUsernamePrompt = false;
                ctrl.toggleEmailPrompt = toggleEmailPrompt;
                ctrl.toggleUsernamePrompt = toggleUsernamePrompt;
            } //DOM manipulation
        }
    }
]);