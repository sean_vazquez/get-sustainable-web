/*
 *  Subscribe Module Service
 *      - Service caters to those who want a subscribtion check or creation
 */

angular.module('app').service('SubscribeService', ['$http', '$q', 'Subscribe',
    function ($http, $q, Subscribe) {
        var subscribeService = this;
        subscribeService.statusLibrary = [
            {
                statusText: 'pending',
                subcribed: false
            },
            {
                statusText: 'registered and not subscribed',
                subcribed: false
            },
            {
                statusText: 'subscribed',
                subcribed: true
            }
        ];
        subscribeService.createSubscription = function (email) {
            //Create the User Susbscription, but enter them as not subscribed until subcription is confirmed.
            return Subscribe.create({
                email: email,
                status: 0
            }, function (result) {
                alert('You have been subscribed!!! ' + result.email);
                return "You've been subscribed!!!";
            }, function (result) {
                alert('Creation could not be completed at this time. ' + result.statusText);
                return "Creation could not be completed at this time."
            });
        };
        subscribeService.checkSubscription = function (email) {
            return Subscribe.findOne({
                filter: { where: email }
            }, function (result) {
                var message = subscribeService.statusLibrary[result.status].statusText;
                alert("Your subscription is currently " + message + " !"); 
            }, function (result) {
                //console.log('Email not found', result);
                //console.log('Since we dont have a record of you, lets create', email);
                if (result && result.status == 404) {
                    return subscribeService.createSubscription(email.email);
                }
            });
        };
        subscribeService.subscribeNew = function (email) {
            //
            //Search for a subcription and create
            //
            return subscribeService.checkSubscription({ email: email });
        };
    }
]);