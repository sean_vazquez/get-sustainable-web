/*
 *  Stats Directive Module
 *      - runs stats utilities
 */
angular.module('app').directive('statsArticle', [
    function () {
        return {
            scope: true,
            templateUrl: "modules/articles/stats/stats.template.html",
            controller: 'StatsArticleController'
        }
    }
]).controller('EditArticleController',['$scope', 
    function ($scope) {
        //console.log($scope.sw.user);
        $scope.getUserInfo = function (user) {
        };
}]);