/*
 *  View Directive Module
 *      - runs view utilities
 */
angular.module('app').directive('viewArticle', [
    function () {
        return {
            scope: true,
            templateUrl: "modules/articles/view/view.template.html",
            controller: 'StatsArticleController'
        }
    }
]).controller('ViewArticleController',['$scope', 
    function ($scope) {
        //console.log($scope.sw.user);
        $scope.getUserInfo = function (user) {
        };
}]);