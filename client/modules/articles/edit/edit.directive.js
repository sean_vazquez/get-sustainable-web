/*
 *  Edit Directive Module
 *      - runs edit utilities
 */
angular.module('app').directive('editArticle', [
    function () {
        return {
            scope: true,
            templateUrl: "modules/articles/edit/edit.template.html",
            controller: 'EditArticleController'
        }
    }
]).controller('EditArticleController',['$scope', 'EditArticleService', 
    function ($scope, EditArticleService) {
        
        /*
         *  Our Article Object
         */
        $scope.article = {
            title: "",
            category: "",
            keywords: "",
            text: ""
        };
        
        /*
         *  Edit Or Create an Article
         */
        function ArticleEditOrCreate(article) {
            EditArticleService.edit(article, function (response) {
                alert(response);
            }, function (error) {
                alert(error);    
            });
        }
        
        /*
         *  Check Validity of Article Object
         */
        $scope.article.isValid = {
            addTitleCat: 
                ($scope.article.title + 
                $scope.article.category).length > 0,
            addTextKey: 
                ($scope.article.keywords + 
                $scope.article.text).length > 0
        };
        
        //Keywords for Articles
        //
        //
}]);