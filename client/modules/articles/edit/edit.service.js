/*
 *  Edit Module Service
 *      - Service caters to those who want an edit check or creation
 */

angular.module('app').service('EditArticleService', ['$http', '$q', 'Article',
    function ($http, $q, Article) {
        //What is this for?
        var what = 'article';
        //Our Edit Service
        var EditService = this;
        //Create Article
        EditService.createArticle = function (user, article) {
            //Create Time Now
            var d = new Date();
            var time = d.getTime();
            //Create the User Susbscription, but enter them as not subscribed until subcription is confirmed.
            return Article.create({
                title: article.title,
                text: article.text,
                category: article.category,
                image: article.mainImage,
                thumbnail: article.thumbImage,
                last: time,
                created: time,
                user: user.id,
                keywords: article.keywords
            }, function (result) {
                //Refactor into language and alert service
                var successMessage = ('Your ' + what + 'has been added! Completed by: ' + result.byEmail);
                alert(successMessage);
                //Refactor into language and alert service
                return successMessage;
            }, function (result) {
                //Refactor into language and alert service
                var errorMessage = ('Your ' + what + ' could not be added at this time. Tried by: ' + byEmail + ' Error Status: ' + result.statusText);
                alert(errorMessage);
                //Refactor into language and alert service
                return errorMessage;
            });
        };
        //Create Article Title Exists
        EditService.checkArticleTitle = function (article) {
            //Build out Term Clauses for article titleSearch
            var termClauses = article.title.split(/\s+/).map(function (t) {
                return {
                    title: {
                        like: t,
                        options: 'i'
                    }
                };
            });
            //Search with 'like' and 'or' terms
            return Article.find({
                filter: {
                    where: {
                        or: termClauses
                    }
                }
            }, function (results) {
                console.log(results);
                alert("Your " + what + " is similar to " + results + " !");
            }, function (results) {
                //console.log('Articles not found', result);
                //console.log('Since we dont have a record of your article, lets create one ', article.user.fname);
                //if (result && result.status == 404) {
                //    return EditService.createSubscription(email.email);
                //}
                return EditService.createArticle(article);
            });
        };
        //Edit Article
        EditService.edit = function (article) {
            //
            //Search for a subcription and create
            //
            return EditService.checkArticleTitle(article);
        };
    }
]);