/*
 *  Login Module Service
 *      - Service caters to those who want a subscribtion check or creation
 */

angular.module('app').service('LoginService', ['$http', '$q', 'SusUser',
    function ($http, $q, SusUser) {
        var loginService = this;
        /*
        Maybe we can ask the user to create a profile
        loginService.createSubscription = function (email) {
            return Subscribe.create({
                email: email,
                issub: true
            }, function (result) {
                alert('You have been subscribed!!! ' + result.email);
                return "You've been subscribed!!!";
            }, function (result) {
                alert('Creation could not be completed at this time. ' + result.statusText);
                return "Creation could not be completed at this time."    
            });
        };
        */
        loginService.LoginUserCred = function (user) {
            return SusUser.login(user, 
                function (result) {
                    alert("You've Logged In!!! " + result.email);
                    return "You've Logged In!!!";
                }, function (result) {
                console.log('System is down', result);
            });
        };
        loginService.checkUser = function (user) {
            return SusUser.findOne({ 
                filter: { where: user.email } 
            }, function (result) {
                if(result.email == email) {
                    return loginService.LoginUserCred(user);
                } else {
                    //Not sure really what that means
                }
            }, function (result) {
                if(result && result.status == 404) {
                    console.log('Email not found', result);  
                }
            });
        };
        loginService.loginUser = function (user) {
            //
            //Search for a subcription and create
            //
            return loginService.checkUser(user);
        };
    }
]);