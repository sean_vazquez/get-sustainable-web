/*
 *  Login Directive Module
 *      - runs menu utilities
 */
angular.module('app')
    .directive('login', [
        function () {
            return {
                scope: true,
                templateUrl: "modules/menu/mainMenu.template.html",
                controller: 'LoginController',
                //Embed a custom controller in the directive
                link: function (scope, element, attrs) {
                    //Input check and prompts
                    var ctrl = this;
                    var toggleEmailPrompt = function (value) {
                        ctrl.showEmailPrompt = value;
                    };

                    var toggleUsernamePrompt = function (value) {
                        ctrl.showUsernamePrompt = value;
                    };

                    ctrl.showEmailPrompt = false;
                    ctrl.showUsernamePrompt = false;
                    ctrl.toggleEmailPrompt = toggleEmailPrompt;
                    ctrl.toggleUsernamePrompt = toggleUsernamePrompt;
                } //DOM manipulation
            }
        }
    ])
    .controller('LoginController',['$scope', 'LoginService',
    function ($scope, LoginService) {
        $scope.user = {
            email: '',
            password: ''
        };
        //Login User
        $scope.loginUser = function (user) {
            var loginObj = user;
            LoginService.loginUser(loginObj, 
            function (response) {
                console.log(response);   
            });
        };
}]);