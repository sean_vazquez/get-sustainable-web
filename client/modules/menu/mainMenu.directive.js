/*
 *  Menu Directive Module
 *      - runs menu utilities
 */
angular.module('app').directive('mainMenu', [
    function () {
        return {
            scope: true,
            templateUrl: "modules/menu/mainMenu.template.html",
            controller: 'MainMenuController'
        }
    }
]).controller('MainMenuController',['$scope', 
    function ($scope) {
        //console.log($scope.sw.user);
        $scope.getUserInfo = function (user) {
        };
}]);