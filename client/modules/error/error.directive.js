/*
 *  Error Directive Module
 *      - runs error interface
 */
var scripts = document.getElementsByTagName("script")
var currentScriptPath = scripts[scripts.length - 1].src;

angular.module('app').directive('subscribeModule', ['$q', 'SubscribeService',
    function ($q, SubscribeService) {
        return {
            templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1)
            + 'error.template.html',
            //Embed a custom controller in the directive
            //controller: SubscribeModuleController,
            link: function ($scope, element, attrs) {
                
            } //DOM manipulation
        }
    }
]);