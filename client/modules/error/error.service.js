(function () {
    angular.module('app')
        .service('ErrorService', function (Error) {
            var errorService = this;
            errorService.processError = function (error) {
                Error.create({
                    title: error.title,
                    description: error.title
                }).$promise.then(function (response) {
                    alert(error);
                });
            }
        });
})();